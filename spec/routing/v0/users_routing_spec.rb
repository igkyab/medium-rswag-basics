require "rails_helper"

RSpec.describe V0::UsersController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/v0/users").to route_to("v0/users#index")
    end

    it "routes to #show" do
      expect(get: "/v0/users/1").to route_to("v0/users#show", id: "1")
    end
    
    it "routes to #create" do
      expect(post: "/v0/users").to route_to("v0/users#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/v0/users/1").to route_to("v0/users#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/v0/users/1").to route_to("v0/users#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/v0/users/1").to route_to("v0/users#destroy", id: "1")
    end
  end
end
