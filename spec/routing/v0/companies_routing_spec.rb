require "rails_helper"

RSpec.describe V0::CompaniesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/v0/companies").to route_to("v0/companies#index")
    end

    it "routes to #show" do
      expect(get: "/v0/companies/1").to route_to("v0/companies#show", id: "1")
    end

    it "routes to #create" do
      expect(post: "/v0/companies").to route_to("v0/companies#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/v0/companies/1").to route_to("v0/companies#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/v0/companies/1").to route_to("v0/companies#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/v0/companies/1").to route_to("v0/companies#destroy", id: "1")
    end
  end
end
