FactoryBot.define do
  factory :company do
    name { Faker::Company.name }
    email { Faker::Internet.email }
    logo { Faker::Company.logo }
  end
end
